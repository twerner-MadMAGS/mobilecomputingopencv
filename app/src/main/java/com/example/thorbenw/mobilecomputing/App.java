package com.example.thorbenw.mobilecomputing;


import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import android.widget.ImageView;


/**
 * Created by thorbenw on 6/7/16.
 */
public class App {
    private MainActivity host;
    private Rect rect;

    private Scalar red = new Scalar(255,0,0);
    private Scalar green = new Scalar(0,255,0);
    private Scalar blue = new Scalar(0,0,255);
    private Scalar white = new Scalar(255,255,255);

    public App(MainActivity host) {
        this.host = host;
        //our testing object
        rect = new Rect(100, 100, 100, 100);
    }

    Mat f  = new Mat();
    public Mat update(Mat frame, Hand hand) {
        //f = frame.clone();
        frame.copyTo(f);

        //Draw white background
        Imgproc.rectangle(f,new Point(0,0), new Point(host.frameWidth, host.frameHeight), white, host.frameWidth);

        //draw our box
        Imgproc.rectangle(f, rect.br(), rect.tl(), blue, 5);

        if(hand != null) {
            Point position = hand.center;
            boolean grabbed = hand.grab;
            if (grabbed) {
                //adjust position of box
                if (rect.contains(position)) {
                    rect.x = (int) position.x - rect.width / 2;
                    rect.y = (int) position.y - rect.height / 2;
                }
                //draw hand position
                Imgproc.circle(f, position, 20, red, 5);
            } else {
                Imgproc.circle(f, position, 20, green, 5);
            }
        }

        return f;
    }


}