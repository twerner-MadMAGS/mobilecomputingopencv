package com.example.thorbenw.mobilecomputing;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

/**
 * Created by thorbenw on 3/17/16.
 */
public class BackgroundFilter {
    public int backgroundCalibrationCount;
    private Mat background;

    public BackgroundFilter() {
        backgroundCalibrationCount = 100;
        background = null;
    }

    Mat m;
    public Mat removeBackground(Mat frame) {
        m = frame.clone();

        if(background == null) {
            background = frame.clone();
        }

        //creates an average of 100 frames that contruct the background
        if(backgroundCalibrationCount > 0) {
            Core.addWeighted(frame, 0.1, background, 0.9, 0.0, background);
            backgroundCalibrationCount--;
        }
        else {//if calibration is finished substract the background and return
            Core.absdiff(background, m, m);
            Imgproc.threshold(m, m, 10, 255, Imgproc.THRESH_BINARY + Imgproc.THRESH_OTSU);
        }

        return m;
    }
}
