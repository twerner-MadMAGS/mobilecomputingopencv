package com.example.thorbenw.mobilecomputing;

import android.util.Log;

import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * Created by thorbenw on 6/2/16.
 */
public class ContourFilter {
    MainActivity host;


    public ContourFilter(MainActivity h) {
        host = h;
    }

    public Vector<Point> findConvexPoints(List<MatOfPoint> contours) {
        List<MatOfPoint> contourCandidates = new ArrayList<>();

        //filter out small contours and sort by size
        for(int i = 0; i < contours.size(); i++) {
            if (Imgproc.contourArea(contours.get(i)) > host.minimalContourArea){
                if(contourCandidates.size() == 0) {
                    contourCandidates.add(contours.get(i));
                }
                else {
                    for(int a = 0; a < contourCandidates.size(); a++) {
                        if(a == contourCandidates.size()-1 || Imgproc.contourArea(contours.get(i)) > Imgproc.contourArea(contourCandidates.get(a))) {
                            contourCandidates.add(a, contours.get(i));
                            break;
                        }
                    }
                }
            }
        }
        //Log.d("ContourCandidates", contourCandidates.size()+"");

        //filter out the relevant points of the contour
        Vector<Point> relevantPoints = new Vector<>();
        for(int i=0; i < contourCandidates.size();i++) {
            MatOfInt indices = new MatOfInt();
            Imgproc.convexHull(contourCandidates.get(i), indices, false);

            relevantPoints = filterConvexPoints(contourCandidates.get(i), indices);
        }
        return relevantPoints;
    }

    private Vector<Point> filterConvexPoints(MatOfPoint contour, MatOfInt indices) {
        Vector<Point> relevantPoints = new Vector<>();
        int minX = host.frameWidth;
        int maxX = 0;
        int avrgX = 0;
        int avrgY = 0;

        //compute center of point cloud
        //aswell as minimal X and maximal X
        int index;
        Point pt;
        for (int j = 0; j < indices.size().height; j++) {
            index = (int) indices.get(j, 0)[0];
            pt = new Point((int) contour.get(index, 0)[0], (int) contour.get(index, 0)[1]);
            minX = Math.min(minX, (int)pt.x);
            maxX = Math.max(maxX, (int)pt.x);
            avrgX += pt.x;
            avrgY += pt.y;
        }

        //Distance between thumb and pinky is our threshld
        int threshold = maxX - minX;
        threshold = (int)(threshold/1.5);
        Point center = new Point((int)(avrgX/indices.size().height), (int)(avrgY/indices.size().height));

        Rect rect = new Rect(
                new Point(center.x - threshold, center.y - threshold),
                new Point(center.x + threshold, center.y + threshold));


        //every point that lays outside of out threshold rectangle is ignored
        //this filters out the point where the arm enters the picture
        //this point is also a point of the convex hull, but should not be considered
        for (int j = 0; j < indices.size().height; j++) {
            index = (int) indices.get(j, 0)[0];
            pt = new Point((int) contour.get(index, 0)[0], (int) contour.get(index, 0)[1]);
            if(rect.contains(pt)) {
                relevantPoints.add(pt);
            }
        }

        return relevantPoints;
    }
}
