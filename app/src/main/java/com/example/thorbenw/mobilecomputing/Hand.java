package com.example.thorbenw.mobilecomputing;


import android.util.Log;

import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.util.Vector;

/**
 * Created by thorbenw on 4/28/16.
 */
public class Hand {
    public boolean grab;

    public Point thumb;
    public Vector<Point> fingerPoints;
    public boolean rightHand;
    public Point center;

    public Hand() {
        thumb = null;
        fingerPoints = new Vector<>();
        rightHand = false;
        center = null;
        grab = false;
    }

    public Hand(boolean g, Point thumb, Vector<Point> fp, boolean rightHand, Point c) {
        this.rightHand = rightHand;
        this.thumb = thumb;
        fingerPoints = new Vector<>();
        if(fp != null) {
            fingerPoints.addAll(fp);
        }
        center = c;
        grab = g;
    }


    public static Hand computeAverage(Vector<Hand> hands, Vector<Integer> weights) {
        boolean isRightHand = hands.firstElement().rightHand;
        boolean isGrab = hands.firstElement().grab;
        int thumbX = 0;
        int thumbY = 0;
        int[] fingerX = new int[hands.firstElement().fingerPoints.size()];
        int[] fingerY = new int[hands.firstElement().fingerPoints.size()];
        int centerX = 0;
        int centerY = 0;


        if (hands.size() == weights.size() && weights.size() > 1) {
            for (int i = 0; i < hands.size(); i++) {
                if (hands.get(i).rightHand != isRightHand) {
                    return null;
                }

                if(hands.get(i).thumb != null) {
                    thumbX += hands.get(i).thumb.x * weights.get(i);
                    thumbY += hands.get(i).thumb.y * weights.get(i);
                }

                centerX += hands.get(i).center.x * weights.get(i);
                centerY += hands.get(i).center.y * weights.get(i);

                for (int a = 0; a < fingerX.length; a++) {
                    fingerX[a] += hands.get(i).fingerPoints.get(a).x * weights.get(i);
                    fingerY[a] += hands.get(i).fingerPoints.get(a).y * weights.get(i);
                }
            }

            int weightTotal = 0;
            for (Integer i : weights) {
                weightTotal += i;
            }

            Vector<Point> fingers = new Vector<>();
            for (int i = 0; i < fingerX.length; i++) {
                fingers.add(new Point(fingerX[i] / weightTotal, fingerY[i] / weightTotal));
            }

            return new Hand(
                    isGrab,
                    new Point(thumbX / weightTotal, thumbY / weightTotal),
                    fingers,
                    isRightHand,
                    new Point(centerX / weightTotal, (isGrab ? centerY : thumbY) / weightTotal)
            );

        }
        return null;
    }

    public void draw(Mat frame, int lineWidth) {
        //draw thumb
        if(thumb != null) {
            Imgproc.circle(
                    frame,
                    thumb,
                    20,
                    new Scalar(255, 0, 0),
                    lineWidth
            );
        }

        //draw fingers
        if(fingerPoints.size() > 0) {
            for (Point p : fingerPoints) {
                Imgproc.circle(
                        frame,
                        p,
                        20,
                        new Scalar(0, 255, 0),
                        5
                );
            }
        }

        //draw center
        if(center != null) {
            if(!grab) {
                Imgproc.circle(
                        frame,
                        center,
                        20,
                        new Scalar(0, 0, 255),
                        lineWidth
                );
            }
            else {
                Imgproc.circle(
                        frame,
                        center,
                        30,
                        new Scalar(255, 0, 0),
                        lineWidth + 3
                );
            }
        }
    }
}
