package com.example.thorbenw.mobilecomputing;

import android.util.Pair;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;


/**
 * Created by thorbenw on 5/26/16.
 */
public class HandFactory {
    MainActivity host;

    //used to create the thresholds, needed for grouping points
    private int xDivider = 5;
    private int yDivider = 1;
    private int xThreshold;
    private int yThreshold;

    private float groupingFactorTreshold = 0.5f;

    private int historyDepth = 10;

    //the last 10 frames and Hands if we found one
    private Vector<Hand> handHistory = new Vector<>();
    private Vector<Hand> fistHistory = new Vector<>();
    //the last 10 frames and how many convex points have been grouped to a average finger point
    //used to find grabs
    private Vector<Float> gfHistory = new Vector<>();


    //is used as a reference to find open to grad transition
    private Float snapshot = null;

    public HandFactory(MainActivity host) {
        this.host = host;
    }

    public Hand getNewHand(Vector<Point> points) {
        //every finger has 2-3 convex points
        //we group those to one average point per finger
        Vector<Point> groupedPoints = groupPoints(points);

        //try to find hands and put those into the histories
        //can be null
        addOpenHandData(groupedPoints);
        addFistData(groupedPoints);

        Hand h = null;

        //try to find an open hand in handHistory
        //if found, return
        h = interpolateOpen();
        if (h != null)
            return h;

        //try to find a closed hand in fistHistory
        //if found return
        //open hands are prioritised over closed ones, because of stronger evidence for open ones
        h = interpolateClosed();
        if(h != null)
            return h;

        //if nothing is found return null
        //histories are updated anyway
        snapshot = null;
        return null;
    }


    private boolean addFistData(Vector<Point> groupedPoints) {
        Hand h = null;
        if(checkForGrab()) {
            //if checkForGrab returns true
            //create a hand with only a center and put into fistHistory
            if (groupedPoints.size() > 0) {
                int avrgX = 0;
                int avrgY = 0;
                for (Point p : groupedPoints) {
                    avrgX += p.x;
                    avrgY += p.y;
                }
                avrgX /= groupedPoints.size();
                avrgY /= groupedPoints.size();

                h = new Hand();
                h.grab = true;
                h.center = new Point(avrgX, avrgY);
            }

        }
        fistHistory.add(h);
        if (fistHistory.size() > historyDepth)
            fistHistory.removeElementAt(0);

        return h != null;
    }

    private Hand interpolateClosed() {
        //try to find a fist in the history
        //only reached of no open hand was found
        Hand h = null;

        Pair<Vector<Hand>, Vector<Integer>> historyDataClosed = collectData(fistHistory);
        Hand adjustedFist = null;
        if (historyDataClosed.second.size() > 0) {
            adjustedFist = Hand.computeAverage(historyDataClosed.first, historyDataClosed.second);
        }

        if (adjustedFist != null) {
            h = new Hand();
            h.center = adjustedFist.center;
            h.grab = true;
        }

        return h;
    }


    private boolean addOpenHandData(Vector<Point> possibleFingerPoints) {
        Hand newHand = null;

        if (possibleFingerPoints.size() > 0) {
            //sort points by x
            boolean sorted = false;
            Point cache;
            while (!sorted) {
                sorted = true;
                for (int i = 1; i < possibleFingerPoints.size(); ++i) {
                    if (possibleFingerPoints.get(i - 1).x > possibleFingerPoints.get(i).x) {
                        sorted = false;
                        cache = possibleFingerPoints.get(i - 1);
                        possibleFingerPoints.remove(i - 1);
                        possibleFingerPoints.insertElementAt(cache, i);
                    }
                }
            }

            //setThresholds
            double xDiff = possibleFingerPoints.lastElement().x - possibleFingerPoints.firstElement().x;
            xThreshold = (int) xDiff / xDivider;
            yThreshold = (int) xDiff / yDivider;

            //group points by x
            HashMap<Double, Vector<Point>> longitudeMap = new HashMap<>();
            double minKey = host.frameWidth;
            double maxKey = 0;

            for (Point point : possibleFingerPoints) {
                boolean pointAsserted = false;
                for (Map.Entry<Double, Vector<Point>> entry : longitudeMap.entrySet()) {
                    double key = entry.getKey();
                    minKey = Math.min(key, minKey);
                    maxKey = Math.max(key, maxKey);
                    //group points by X value
                    if (point.x > key - xThreshold && point.x < key + xThreshold) {
                        entry.getValue().add(point);
                        pointAsserted = true;
                        break;
                    }
                }
                if (!pointAsserted) {
                    Vector<Point> v = new Vector<>();
                    v.add(point);
                    longitudeMap.put(point.x, v);
                    minKey = Math.min(point.x, minKey);
                    maxKey = Math.max(point.x, maxKey);
                }
            }

            /* Draw the thresholds used to identify thumbs
            Imgproc.rectangle(
                    host.frameFlipped,
                    new Point(minKey - xThreshold, 0),
                    new Point(minKey + xThreshold, host.frameHeight),
                    new Scalar(0, 255, 0),
                    3
            );
            Imgproc.rectangle(
                    host.frameFlipped,
                    new Point(maxKey - xThreshold, 0),
                    new Point(maxKey + xThreshold, host.frameHeight),
                    new Scalar(0, 255, 0),
                    3
            );
            */

            Vector<Point> possibleThumbs = new Vector<>();
            //the very left and very right group contain possible thumbs
            if (longitudeMap.get(minKey).size() == 1)
                possibleThumbs.add(longitudeMap.get(minKey).firstElement());
            if (longitudeMap.get(maxKey).size() == 1)
                possibleThumbs.add(longitudeMap.get(maxKey).firstElement());



            if (possibleThumbs.size() == 1) {
                newHand = new Hand();
                newHand.thumb = possibleThumbs.firstElement();
            } else if (possibleThumbs.size() == 2) {
                //if 2 thumbs are found the lower one is taken
                newHand = new Hand();
                newHand.thumb = possibleThumbs.get(0).y > possibleThumbs.get(1).y ? possibleThumbs.get(0) : possibleThumbs.get(1);
            }

            if (newHand != null) {
                Vector<Point> fingerPoints = new Vector<>();

                //fingers should be positioned in the area over the thumb
                for (Point point : possibleFingerPoints) {
                    if (point.y < newHand.thumb.y && point.y > newHand.thumb.y - yThreshold) {
                        fingerPoints.add(point);
                        Imgproc.circle(
                                host.frameFlipped,
                                point,
                                10,
                                new Scalar(255),
                                3
                        );
                    }
                }

                /*Draw the threshold to find fingers
                Imgproc.rectangle(
                        host.frameFlipped,
                        new Point(0, newHand.thumb.y),
                        new Point(host.frameWidth, newHand.thumb.y - yThreshold),
                        new Scalar(255, 0, 0),
                        3
                );
                */

                //if a thumb was found and if we found another group of points, that has the length 4 we consider them fingers
                if (fingerPoints.size() == 4) {
                    newHand.fingerPoints.addAll(fingerPoints);
                    int avrgX = 0;
                    for(Point p : fingerPoints) {
                        avrgX += p.x;
                    }
                    newHand.center = new Point(avrgX/fingerPoints.size(), newHand.thumb.y);
                }
                else {
                    newHand = null;
                }
            }
        }

        //even if no hand was found we update the history
        //missing hands are also information
        handHistory.add(newHand);
        if (handHistory.size() > historyDepth)
            handHistory.removeElementAt(0);

        return newHand != null;
    }

    private Hand interpolateOpen() {
        Pair<Vector<Hand>, Vector<Integer>> historyDataOpen = collectData(handHistory);

        int totalWeight = 0;
        for(Integer i : historyDataOpen.second) {
            totalWeight += i;
        }

        //only a high total weight results in a hand
        //e.G. the newest 3 Frames contain a hand
        if (totalWeight > 25) {
            Hand adjustedHand = Hand.computeAverage(historyDataOpen.first, historyDataOpen.second);
            if(adjustedHand != null) {
                return adjustedHand;
            }
        }
        return null;
    }


    private boolean checkForGrab() {
        //find transition frame
        //the oldest frame without a hand
        int transitionFrameIndex = -1;
        for(int i = handHistory.size()-1; i > 0; i--) {
            if(handHistory.get(i) != null || i == 1) {
                transitionFrameIndex = i-1;
                break;
            }
        }

        //create reference value out of every frame before the transition frame
        if(snapshot == null && transitionFrameIndex >= 0 && transitionFrameIndex < historyDepth - 3) {
            createSnapshot(transitionFrameIndex);

            if(snapshot != null) {
                int totalWeight = 0;

                float avrgGF = 0;
                for(int i = transitionFrameIndex; i < gfHistory.size(); i++) {
                    totalWeight += i;
                    avrgGF += gfHistory.get(i) * i;
                }

                if(totalWeight > 0) {
                    avrgGF /= totalWeight;
                    //if fingers get close to each other more convex points get grouped together
                    //=> fist
                    if (avrgGF > snapshot + groupingFactorTreshold) {
                        //grab is 'found'
                        return true;
                    }
                }
            }

            snapshot = null;
        }

        return snapshot != null;
    }

    private void createSnapshot(int range) {
        float avrgGF = 0;
        int totalWeight = 0;

        for (int i = 0; i < range; i++) {
            if(handHistory.get(i) != null) {
                avrgGF += gfHistory.get(i) * i;
                totalWeight += i;
            }
        }
        if (totalWeight > 0)
            snapshot = avrgGF / totalWeight;
    }

    private Vector<Point> groupPoints(Vector<Point> allPoints) {
        Vector<Vector<Point>> groupedPoints = new Vector<>();

        Point pt;
        //group points by distance to each other
        for (int j = 0; j < allPoints.size(); j++) {
            pt = allPoints.get(j);
            boolean pointSorted = false;
            for (Vector<Point> points : groupedPoints) {
                if (points.size() > 0) {
                    //TODO; MAGIC NUMBERS
                    if (Math.sqrt(Math.pow(pt.x - points.lastElement().x, 2) + Math.pow(pt.y - points.lastElement().y, 2)) < host.frameWidth / 30) {
                        points.add(pt);
                        pointSorted = true;
                        break;
                    }
                }
            }
            if (!pointSorted) {
                Vector<Point> v = new Vector<>();
                v.add(pt);
                groupedPoints.add(v);
            }
        }

        //construct average points out of the groups
        float avrgGroupSize = 0;
        Vector<Point> possibleFingerPoints = new Vector<>();
        for (Vector<Point> points : groupedPoints) {
            int x = 0;
            int y = 0;
            for (Point p : points) {
                x += p.x;
                y += p.y;
            }
            x /= points.size();
            y /= points.size();

            avrgGroupSize += points.size();

            possibleFingerPoints.add(new Point(x, y));

            Imgproc.circle(host.frameFlipped, new Point(x, y), 5, new Scalar(255, 0,0), 5);
        }

        gfHistory.add(avrgGroupSize / groupedPoints.size());
        if(gfHistory.size() > historyDepth)
            gfHistory.removeElementAt(0);
        //Log.d("grouping factor", avrgGroupSize / groupedPoints.size() + "");

        return possibleFingerPoints;
    }

    private Pair<Vector<Hand>, Vector<Integer>> collectData(Vector<Hand> vec) {
        Vector<Hand> oldHands = new Vector<>();
        Vector<Integer> weights = new Vector<>();

        for (int i = 0; i < vec.size(); i++) {
            if (vec.get(i) != null) {
                oldHands.add(vec.get(i));
                weights.add(i);
            }
        }

        return new Pair<>(oldHands, weights);
    }
}