package com.example.thorbenw.mobilecomputing;

//import android.graphics.Point;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.JavaCameraView;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity implements CameraBridgeViewBase.CvCameraViewListener2 {
    static{ System.loadLibrary("opencv_java3"); }
    public static int maxFrameWidth = 1000;
    public static int maxFrameHeight = 1000;

    public int frameHeight;
    public int frameWidth;

    private Button btnCamera;
    private Button btnApp;
    private Button btnCalibrate;

    private BackgroundFilter backgroundFilter;
    private DisplayMode displayMode;

    private JavaCameraView javaCameraView;

    private HandFactory handFactory;
    private ContourFilter contourFilter;
    private App app;

    public float minimalContourArea = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        javaCameraView = (JavaCameraView) findViewById(R.id.iv);
        javaCameraView.setCvCameraViewListener(this);
        javaCameraView.setVisibility(SurfaceView.VISIBLE);
        javaCameraView.setCameraIndex(JavaCameraView.CAMERA_ID_FRONT);
        javaCameraView.enableFpsMeter();
        javaCameraView.setMaxFrameSize(maxFrameWidth, maxFrameHeight);

        backgroundFilter = new BackgroundFilter();
        displayMode = DisplayMode.Camera;

        btnCamera = (Button) findViewById(R.id.btnCamera);
        btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayMode = DisplayMode.Camera;
            }
        });

        btnApp = (Button) findViewById(R.id.btnApp);
        btnApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayMode = DisplayMode.App;
            }
        });

        btnCalibrate = (Button) findViewById(R.id.btnCalibrate);
        btnCalibrate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backgroundFilter.backgroundCalibrationCount = 100;
            }
        });

        Button btnDebug = (Button) findViewById(R.id.btnDebug);
        btnDebug.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayMode = DisplayMode.Debug;
            }
        });

        contourFilter = new ContourFilter(this);

        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_6, this, opencvLoaderCallback);
    }


    public Mat frameGray = new Mat();
    public Mat frameFlipped = new Mat();
    public Mat frameNoBackground = new Mat();
    public Mat frameApp = new Mat();

    private List<MatOfPoint> contours = new ArrayList<>();
    private Mat hierachy = new Mat();


    @Override
    public Mat onCameraFrame(JavaCameraView.CvCameraViewFrame inputFrame) {
        System.gc();

        Core.flip(inputFrame.rgba(), frameFlipped, 1); //Flips camera image vertically to cancel out mirror effect of the camera

        //only happens on first frame
        if(minimalContourArea == 0) {
            frameHeight = frameFlipped.height();
            frameWidth = frameFlipped.width();
            minimalContourArea = (frameWidth / 8) * (frameHeight / 6);

            handFactory = new HandFactory(this);
            app = new App(this);
        }

        //converts image to grey
        Imgproc.cvtColor(frameFlipped, frameGray, Imgproc.COLOR_RGBA2GRAY, 4);

        frameNoBackground = backgroundFilter.removeBackground(frameGray);
        //try to to reduce noise produced by a too detailed background
        Imgproc.GaussianBlur(frameNoBackground, frameNoBackground, new Size(9,9), 15);

        //contours are stored in 'countours'
        contours.clear();
        Imgproc.findContours(frameNoBackground.clone(), contours, hierachy, Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);

        //this checks for some weird bug, when the image is all zeros e.G. just the background is filmed
        //and aborts
        Hand hand = null;
        if(contours.size() < 600) {
            hand = handFactory.getNewHand(contourFilter.findConvexPoints(contours));
            if (hand != null) {
                hand.draw(frameFlipped, 5);
            }
        }
        //update our app
        frameApp = app.update(frameFlipped, hand);

        //indicator, for when the background is calibrating
        if(backgroundFilter.backgroundCalibrationCount > 0) {
            Imgproc.rectangle(
                    frameFlipped,
                    new Point(0,0),
                    new Point(frameWidth, frameHeight),
                    new Scalar(255,0,0),
                    3
            );
        }

        if(displayMode == DisplayMode.Debug)
            return frameNoBackground;
        if(displayMode == DisplayMode.Camera)
            return frameFlipped;
        else if(displayMode == DisplayMode.App)
            return frameApp;

        return null;
    }



    private enum DisplayMode {
        Camera,
        App,
        Debug
    }

    //Callback class
    //Used by the OpenCV Manager when opencv is loaded
    private BaseLoaderCallback opencvLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.e("opencv", "OpenCV loaded successfully");
                    javaCameraView.enableView();
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };



    @Override
    public void onCameraViewStarted(int width, int height) {

    }

    @Override
    public void onCameraViewStopped() {

    }
}
